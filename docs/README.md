# Keshav11-coder.github.io

## Bootcamp2022
### everything done in the bootcamp in 2022 including : 
#### -_chatapp_ versions 4 - 6, 6 being the final version
#### -_IoT-CLOUD_ : a bootcamp stack createdd by Mr. TheoBoomsma. (still setting up, might not be visible)
#### -_raspberry-pi_ : the folder for all my raspberry pi projects. (still setting up, might not be visible)
> more projects coming in the bootcamp, still sorting stuff out


## TheBoringProject
### TheBoringProject has much more to come, mostly focussed on AI and deep robotics :
#### -_AI_ : for now focussed on simulated self driving, with the best version being 4
#### -_virusSite_ : mostly focussed on the dark side of javascript, not harmfull I dont get any data from the site because I dont know PHP
> the best project I'm working on at the moment, of course more to come in the future, the end result should be finished by 2025

## BoringCam
### a hackotopia 2022 - 2023 project open source display
#### -_new_, upgraded version of an old UI
> work in progress, will be finished around January

## drone-project
### Probably one of the best projects I've made so far, out of pure Node.js, for free.
#### -_wss_. In that folder there's another folder called _ws_, with 2 other folders, _api_ & _app_. The api includes all library files and one example file, while the app folder only hosts a blank page supporting keypresses sent to _the example_.
#### -_AR-drone_. Still work in progress because it's an old projects and some files are missing.
> the best project that was successful.

## 1C 
### just a project for my class, when they don't study and I have to make quizzes for them.
#### -_AK-quiz_, the only quiz so far (written in dutch because I live in a country where they speak dutch). steal the UI, I don't care.
> I dont worry about this at the moment, if it's needed, it's needed. (which is like 99% of the time we have tests).
